openapi: 3.0.0
info:
  version: 0.1.0
  title: Scrapable Execution Server API
  description: The API to deploy and manage Scrapable projects
  license:
    name: MIT
    url: http://opensource.org/licenses/MIT
paths:
  '/projects':
    get:
      summary: Get a summary of all projects
      responses:
        200:
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/definitions/ProjectSummary'
    post:
      summary: Start a new project
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/definitions/ProjectCreationDTO'
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/definitions/ProjectSummary'
  '/projects/{projectId}':
    get:
      summary: Get more information about a specific project
      parameters:
        - name: projectId
          in: path
          required: true
          type: string
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/definitions/Project'
  '/projects/{projectId}/endpoints':
    post:
      summary: Add an endpoint to the project
      parameters:
        - name: projectId
          in: path
          required: true
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/definitions/ApiEndpointCreationDTO'
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/definitions/ApiEndpoint'
    put:
      summary: Update an endpoint
      parameters:
        - name: projectId
          in: path
          required: true
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/definitions/ApiEndpointCreationDTO'
      responses:
        200:
          content:
            application/json:
              schema:
                $ref: '#/definitions/ApiEndpoint'
definitions:
  ProjectSummary:
    type: object
    properties:
      id:
        type: integer
        example: 1234
        readOnly: true
      name:
        type: string
        pattern: "[a-z0-9\\-_]"
        example: finance-scraper
      status:
        type: string
        enum: 
          - running
          - stopped
          - no_deployment_yet
      errors:
        type: integer
      pending_deployment:
        type: boolean
  ProjectCreationDTO:
    type: object
    properties:
      name:
        type: string
        pattern: "[a-z0-9\\-_]"
        example: finance-scraper
  Project:
    type: object
    allOf:
      - $ref: '#/definitions/ProjectSummary'
    properties:
      endpoints:
        type: array
        items:
          $ref: '#/definitions/ApiEndpoint'
  ApiEndpoint:
    allOf:
      - $ref: '#/definitions/ApiEndpointCreationDTO'
    properties:
      errors:
        type: integer
  ApiEndpointCreationDTO:
    type: object
    properties:
      name:
        type: string
        pattern: "[a-z0-9\\-_]"
        example: get_current_balance
      method:
        type: string
        enum:
          - GET
          - POST
          - PUT
          - DELETE
      path:
        type: string
        example: "/current_balance"